package mx.ssf

object Recursion {
  
  def factorial(n: Int): Int = ???
  
  def sumAllElements(list: List[Int]): Int = ???
  
  def max(list: List[Int]): Int = ???
  
  def min(list: List[Int]): Int = ???
  
  def avg(list: List[Int]): Int = ???
  
  def reverse(list: List[Int]): List[Int] = ???
  
  //Finds the position in the List where the element is found or -1 if the element is not in the List
  def findFirst(list: List[Int], value: Int): Int = ???
   
  def isSortedAsc(list: List[Int]): Boolean = ???
     
  def isSortedDesc(list: List[Int]): Boolean = ???
  
  def isSortedAscByLenght(list: List[String]): Boolean = ???
  
  def isSortedByLength(list: List[String]): Boolean = ???
  
  //This should work as sum all elements but with a first element as accumulator.
  //This could be solved using sumAllElements(list) + first but try to implement this without using sumAllElements
  //That way implementing a generic fold function later will be easy
  def foldInts(first: Int ,list: List[Int]): Int = ???
  
  def isPrime(number: Int): Boolean = ???

  
  def sort(list: List[Int]): List[Int] = ???
    
}