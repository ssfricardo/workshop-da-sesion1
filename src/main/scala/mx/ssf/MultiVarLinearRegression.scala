package mx.ssf

import scala.util._
import breeze.linalg._
import breeze.numerics._

object MultiVarLinearRegression {

  /**
   * Implementación de gradiente decendiente utilizando algebra lineal y fijado a regresiones lineales multivariable
   * con función de costo basada en el promedio cuadrado de los errores.
   * Pregunta: Con lo ya aprendido: ¿Cómo harías una función gradiente descendiente que pueda trabajar con regresiones
   * polinomiales y con funciones de costo distintas?
   */
  def ∇(θ: DenseVector[Double], ts: DenseMatrix[Double],
        α: Double, convergenceRate: Double) = ???

}