package mx.ssf

import org.joda.time._
import org.joda.time.format._
import scala.util._

object Challenge {
  
  
  private val DVENTAS_RAW_LOCATION = """C:\Users\ricar\Downloads\csv\dventas.csv"""
  
  val getAllDVentas: Iterator[DVentas] = csvToObject(DVENTAS_RAW_LOCATION, DVentas.parse)

  /**
   * Leemos el archivo con los datos y lo colocamos en un Iterador que tiene las líneas del archivo
   */
  private def readData(fileLocation: String): Iterator[String] = io.Source.fromFile(fileLocation).getLines

  /**
   * Se debe eliminar la primer línea leída ya que es el nombre de las columnas, no son datos.
   * Posteriormente debemos de crear para cada línea un arreglo de Strings haciendo un split por las comas
   * para tener los datos separados.
   * No nos olvidamos de hacer finalmente un trim a cada dato para evitar espacios innecesarios.
   */
  private def cleanData(data: Iterator[String]): Iterator[Array[String]] =
    data.drop(1).map(_.replace("\"", "")).map(_.split(";").map(_.trim))
  
  private def csvToObject[A](pathToFile: String, parserFunction: Array[String] => Try[A]): Iterator[A] =
    cleanData(readData(pathToFile)).map( parserFunction ).collect { case Success(x) => x }
     
    /**
     * This function must return the gross sales per year in a Tuple2 where the first element of the tuple is the year
     * and the second element is the gross sale without decimals. The list must be sorted by year
     * being the first elements of the list the most recent year Ej. List(("2016", 348485),...)
     */
    def getGrossSales(dventas: Iterator[DVentas]): List[(String, Int)] = ???
	      
	  /**
	   * Return a map where Map[Year, (ProductName, #PiecesSold, ProductGrossSale, %ofGrossSales)]
	   * %ofGrossSales = ProductGrossSale / grossSales * 100
	   */
    def getTopProductsByYear(top: Int, year: String): Map[Int,(String, Int, Int, Int)] = ???
    
    /**
     * Get the % of sales per month using all the history.
     * Is there monthly seasonality? Whats the best year?
     */
    def getSeasonalityByMonth(): List[(String,Float)] = ???
    
    /**
     * Get the % of sales per day usign all history
     * What's the best day of the week for the restaurant.
     */
    def getSeasonalityByDay(): List[(String,Float)] = ???
    
    /**
     * List[(year,average price)]
     */
    def getTheAveragePricePerYear(): List[(String, Float)] = ???
    
    /**
     * Is the business growing in sales?
     */
    def getTheSalesIncrementByYear: List[(String, Float)] = ???
    
    /**
     * Is the business is becoming more expensive?
     */
    def getThePorcentualYearIflation(): List[(String,Float)] = ???
    
    /**
     * Are the tastes of the people changing over time
     * Calculate it getting the top ten products by year and calculating its frequency by year.
     * What's the top ten of all history grouping by year? Do you get close to 10 products, you get close to 40 products?
     */
    def compareSalesGrowthVsPriceInflation: List[(String,Int)] = ???
    
    /**
     * The owner of the restaurant is requesting that you get any information from the data that could help him
     * take informed actions. Think one KPI using basic math, the data and the concepts learned in this workshop
     */
    def yourKPI1 = ???
    
    /**
     * The owner of the restaurant is requesting that you get any information from the data that could help him
     * take informed actions. Think one KPI using basic math, the data and the concepts learned in this workshop
     */
    def yourKPI2 = ???
    
}

    class DVentas(val almacen: Int, val numOrden: Int, val sku: String, val cantidad: Double, val precioUnitario: Double, val precioTotal: Double, val date: DateTime)

    object DVentas{
    
      def parse(rawData: Array[String]): Try[DVentas] =  
          Try{
            new DVentas(
               rawData(0).toInt, 
               rawData(1).toInt, 
               rawData(2), 
               rawData(3).toDouble, 
               rawData(4).toDouble, 
               rawData(5).toDouble,
               DateTime.parse(rawData(6), DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss")))
          }
}