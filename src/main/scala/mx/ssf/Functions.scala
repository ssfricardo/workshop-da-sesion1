package mx.ssf

object Functions {
  
  //No usar funciones de las clases Iterator, Array o List. Implementar con recursión. Las funciones
  //Permitidas de la clase List son :+ y +: únicamente.
  
  /**
   * Should return true if the ordering function provided complains to all elements in the list
   */
  def isSorted[A](list: List[A], ordering: (A,A) => Boolean): Boolean = ???
  
  def fold[A](first: A, list: List[A], f: (A,A) => A): A = ???
  
  def mapList[A,B](list: List[A], f: A => B): List[B] =  ???
  
  def concat[A](list1: List[A], list2: List[A]): List[A] = ???
  
  def flatten[A](listOfLists: List[List[A]]): List[A] = ???
  
  /***
   * If the list is 1::2::3::4::Nil
   * You need to transform into
   * (1,2)::(2,3)::(3,4)::Nil
   * If the list is 1::Nil or Nil
   * You need to transform into
   * Nil
   */
  def toTuples[A](list: List[A]): List[(A,A)] = ???
    
  //Return true if the f function return true to all elements in the list, otherwise return false
  def forAll[A](list: List[A], f: A => Boolean): Boolean = ???
    
  def filter[A](list: List[A], f: A => Boolean) = ???
  
  //If f is true then apply f2 to element else apply f3
  def applyIf[A,B](list: List[A], f: A => Boolean,f2: A => B, f3: A => B) = ???
  
  //Do not use recursion use fold function and list.size
  def average(list: List[Float]): Float = ???
  
  //If the average of all the student grades is lower than 6 then round below othewise roung up.
  //5.8 is 5, 6.5 is 7.
  def calculateStudentGrades(list: List[(String,List[Float])]) : List[(String, Int)] = ???
  
  def isPalindromo(phrase: String): Boolean = ???
  
  def arePalindromos(phrases: List[String]): Boolean = ???
  
  def leaveOnlyPalindromos(phrases: List[String]): List[String] = ???
}