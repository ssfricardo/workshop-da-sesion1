package mx.ssf

import java.io._
import breeze.linalg._
import breeze.numerics._

object DataPreprocessing {

  //This methods are for preparing the testing set to apply machine learning algorithm

  //Return a map where the key is yyyyMM
  def groupSalesByYear(dventas: List[DVentas]): Map[String, List[DVentas]] = ???

  def sumSaleByMonth(dventasPerMonth: Map[String, List[DVentas]]): Map[String, Double] = ???

  def sortSalesByMonth(salesByMonth: Map[String, Double]): List[(String, Double)] = ???

  //Should return 201701, 2934234.23 \n 201702, 39428554.33 ... sorted by date
  def salesByYearToCVS(salesByMonth: Map[String, Double]): String = ???

  //Should return 0.1, 2934234.23 \n 0.2, 39428554.33 ... where 0.1 is the first year
  def scaleTrainingSet(salesByMonth: List[(String, Double)]): List[(Double, Double)] = salesByMonth.toList.sortBy(_._1).zipWithIndex.map(elem => ((elem._2.toDouble / 10), elem._1._2 / 100000)).drop(6)

  //Remove the first 6 elements of the training set.
  def removeElementsOutsideTendency(dventas: List[(Double, Double)]): List[(Double, Double)] = ???

  //Regresar la Matriz [x00,x10,x20,...etc no olvidar x0 son solo unos
  //                    x01,x11,x21,...etc]
  def prepareTrainingSetForMultivariableRegression: DenseMatrix[Double] = ???

  
  //The preprocessing pipeline...
  def prepareTrainingSetForLinearRegression(dventas: List[DVentas]) =
    removeElementsOutsideTendency(
      scaleTrainingSet(
        sortSalesByMonth(
          sumSaleByMonth(
            groupSalesByYear(dventas)))))

  //Damm good by functional programming. Use this method to write the training set to a file
  def csvToFile(csv: String, fileName: String) = {
    val pw = new PrintWriter(new File(fileName))
    pw.write(csv)
    pw.close
  }
}