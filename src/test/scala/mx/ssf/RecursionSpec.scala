package mx.ssf

import org.scalatest._
import org.scalactic.source.Position.apply

class RecursionSpec extends FlatSpec with Matchers {
  "The factorial of 5" should "be 120" in {
    Recursion.factorial(5) shouldEqual 120
  }
  
  "The factorial of 10" should "be 3628800" in {
    Recursion.factorial(10) shouldEqual 3628800
  }
  
  "The sum of 1::2::3::4::Nil" should "be 10" in {
    Recursion.sumAllElements(1::2::3::4::Nil) shouldEqual 10
  }
  
  "The max number in the list" should "be 122" in {
   Recursion.max(12 :: 43 :: 64 :: -10 :: 24 :: 122 :: 33 :: 12 :: 122 :: 1 :: -1 ::Nil) shouldEqual 122
  }
   
  "The min number in the list" should "be -10" in {
    Recursion.min(12 :: 43 :: 64 :: -10 :: 24 :: 122 :: 33 :: 12 :: 122 :: 1 :: -1 ::Nil) shouldEqual -10
  }
  
  "The Average of the numbers 2 :: -2 :: 4 :: 8 :: 12 :: 6 :: Nil" should "be 5" in {
    Recursion.avg(2 :: -2 :: 4 :: 8 :: 12 :: 6 :: Nil) shouldEqual 5
  }
  
  "The reverse list 1::2::3::Nil" should "be 3,2,1" in {
    Recursion.reverse(1::2::3::Nil) shouldEqual 3::2::1::Nil
  }
  
  "The first 5 in the list" should "be in the position 2" in {
    Recursion.findFirst(10::11::5::4::5::Nil, 5) shouldEqual 2
  }
  
  "There's no 8 in the list" should "return -1" in {
    Recursion.findFirst(10::11::5::4::5::Nil, 8) shouldEqual -1
  }
  
  "The list 1::2::3::10::15::Nil is sorted in an incremental way" should "return true" in {
    Recursion.isSortedAsc(1::2::3::10::15::Nil) shouldEqual true
  }
  
  "The list 1::2::3::10::4::15::Nil is not sorted in an incremental way" should "return false" in {
    Recursion.isSortedAsc(1::2::3::10::4::15::Nil) shouldEqual false
  }
  
  "The list 88::55::4::2::-10::Nil is sorted desc" should "return true" in {
    Recursion.isSortedDesc(88::55::4::2::(-10)::Nil) shouldEqual true
  }
  
   "The list 88::55::4::-2::1::-10::Nil is sorted desc" should "return false" in {
    Recursion.isSortedDesc(88::55::4::(-2)::1::(-10)::Nil) shouldEqual false
  }
   
  "The list a::aa::aaa::aaaaaa::Nil is sorted by lenght" should "return true" in{
    Recursion.isSortedByLength( "a"::"aa"::"aaa"::"aaaaaa"::Nil ) shouldEqual true
  }
   
  "The list a::aa::aaaaaaaa::aaaaaa::Nil is not sorted by lenght" should "return true" in{
    Recursion.isSortedByLength( "a"::"aa"::"aaaaaaaa"::"aaaaaa"::Nil ) shouldEqual false
  }
  
  "The fold of 5,1::2::3::Nil" should "be 11" in {
    Recursion.foldInts(5, 1::2::3::Nil) shouldEqual 11
  }
 
  "The first primeNumbers are 2,3,5,7,11,13,17,19,23,29,31,37,41,43,47,53,59,61,67,71,73,79" should "be true" in {
    (2::3::5::7::11::13::17::19::23::29::31::37::41::43::47::53::59::61::67::71::73::79::Nil).forall( Recursion.isPrime(_) )shouldEqual true
  }
  
  "The -1::0::6::8::12::15::25 are not prime numbers" should "be false" in {
    (-1::0::6::8::12::15::25::Nil).forall( !Recursion.isPrime(_) )shouldEqual true
  }
  
  "The list -10::3::55::-199::88::-10::-4::Nil" should "be sorted to -199::-10::-10::-4::3::55::88::Nil" in {
    Recursion.sort(List(-10,3,55,-199,88,-10,-4)) shouldEqual List(-199,-10,-10,-4,3,55,88)
  }
}