import Dependencies._

lazy val root = (project in file(".")).
  settings(
    inThisBuild(List(
      organization := "ssf.mx",
      scalaVersion := "2.12.3",
      version      := "0.1.0-SNAPSHOT"
    )),
    name := "workshop-s1",
    
    libraryDependencies += scalaTest % Test,
    
    libraryDependencies += "joda-time" % "joda-time" % "2.9.9",
    
    // https://mvnrepository.com/artifact/org.scalanlp/breeze
	libraryDependencies += "org.scalanlp" %% "breeze" % "0.13.2"
    
    
    )
